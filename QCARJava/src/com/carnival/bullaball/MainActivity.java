package com.carnival.bullaball;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;

import com.qualcomm.QCARUnityPlayer.QCARPlayerActivity;

public class MainActivity extends QCARPlayerActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	public void testTest() {
		Log.i("Test", "Testing....");
	}
}
