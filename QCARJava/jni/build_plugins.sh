#!/bin/bash
echo "Compiling NativeJavaBridge.cpp..."
ndk-build NDK_PROJECT_PATH=. NDK_APPLICATION_MK=Application.mk $*
mv libs/armeabi/libjavabridge.so ../libs/armeabi-v7a/

echo "Done!"
